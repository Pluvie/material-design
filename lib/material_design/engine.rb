module MaterialDesign
  class Engine < ::Rails::Engine

    # JQuery
    require 'jquery-rails'
    require 'jquery-ui-rails'
    # VueJS
    require 'vuejs-rails'

    # Aggiunge gli helper alla main app
    initializer 'local_helper.action_controller' do
      ActiveSupport.on_load :action_controller do
        helper MaterialDesignHelper
      end
    end

  end
end
