$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "material_design/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "material_design"
  s.version     = MaterialDesign::VERSION
  s.authors     = ["Francesco Ballardin"]
  s.email       = ["francesco.ballardin@gmail.com"]
  s.homepage    = "https://bitbucket.org/Pluvie/material-design.git"
  s.summary     = "MaterialDesign - Kit for Material Design."
  s.description = "MaterialDesign - Kit for Material Design."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  # Rails
  s.add_dependency 'rails', '~> 5.2.1'
  # JQuery
  s.add_dependency 'jquery-rails'
  s.add_dependency 'jquery-ui-rails'
  # VueJS
  s.add_dependency 'vuejs-rails'

end
