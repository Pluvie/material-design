# MaterialDesign
Questa gemma contiene font, css, e javascript pronti all'uso, per poter 'materializzare' il
tuo nuovo sito.

## Installazione
L'installazione è molto semplice.

Aggiungi al Gemfile la riga:
```ruby
gem 'material_design'
```

e dai il comando:
```bash
$ bundle install
```

Poi, nel tuo application.scss, includi la riga:
```css
@import 'material_design';
```

E nel tuo application.js, includi la riga:
```js
//= require 'material_design'
```

Et voilà! Fatto!

## Uso
La gemma fornisce una serie di cose che verranno spiegate a breve.
