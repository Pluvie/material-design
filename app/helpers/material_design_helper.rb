module MaterialDesignHelper

  MATERIAL_DESIGN_TAGS = {
    inputs: [
      :checkbox, :date, :email, :file, :image,
      :number, :password, :radio, :select, :text, :textarea ],
    modals: [
      :confirmation, :destroy, :information ]
  }.freeze

  MATERIAL_DESIGN_TAGS.each do |tag_group, tags|
    tags.each do |tag|
      method_name = "material_design_#{tag_group.to_s.singularize}_#{tag}".to_sym
      vue_method_name = "material_design_vue_#{tag_group.to_s.singularize}_#{tag}".to_sym
      
      define_method method_name do |*args|
        render "shared/#{tag_group}/#{tag}", args: args
      end
      define_method vue_method_name do |*args|
        render "shared/vue/#{tag_group}/#{tag}", args: args
      end
    end
  end

  def info_paragraph(icon)
    render 'shared/info_paragraph', icon: icon do
      yield
    end
  end

end
