/******************************************************************************/
/*                                                                             /*
/*                                                                             /*
/*                           MATERIAL DESIGN                                   /*
/*                                                                             /*
/*                                                                             /*
/******************************************************************************/

// CORE
//= require       jquery
//= require       jquery-ui
//= require       vue
//= require_tree  ./material_design/vues/components
//= require       material_design/core/arrive.min
//= require       material_design/core/popper.min
//= require       material_design/core/moment
//= require       material_design/core/moment.it

// PLUGINS
//= require       material_design/plugins/bootstrap-tagsinput
//= require       material_design/plugins/bootstrap-datepicker
//= require       material_design/plugins/bootstrap-datepicker.it
//= require       material_design/plugins/nouislider.min
//= require       material_design/plugins/jquery.flexisel
//= require       material_design/plugins/jasny-bootstrap.min

// BOOTSTRAP E MATERIAL-KIT
//= require       material_design/bootstrap-material_design
//= require       material_design/material-kit

// CHOSEN
//= require       material_design/chosen.jquery.min

// MATERIAL_DESIGN
//= require       material_design/material_design
