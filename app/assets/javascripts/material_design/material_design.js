/*
 *  MODALI E TOOLTIPS
 * 
 *  Le modali sono di tre tipi:
 *    - destroy: due bottoni, con colorazione rossa, per cancellazioni.
 *    - confirmation: due bottoni, con colorazione primaria, per azioni
 *        che richiedono conferme.
 *    - information: un bottone, per dare solo informazioni senza azioni
 *        da parte dell'utente.
 *                                                                          */

(function($) {
  $(document).ready(function() {
    
    // Modali Cancellazione e Conferma (due bottoni => Annulla o Prosegui)
    $('[data-modal="destroy"], [data-modal="confirmation"]').on('show.bs.modal', function() {
      var $submit = $(this).find('.modal-proceed'),
          href = $submit.attr('href');
      $submit.attr('href', $(this).data('href'));
    });
    $('[data-toggle="modal-destroy"], [data-toggle="modal-confirmation"]').click(function(e) {
      e.preventDefault();
      $($(this).attr('href')).data('href', $(this).data('modal-proceed-href')).modal('show');
    });

    // Modale Informazione (un bottone => Ok)
    $('[data-modal="information"]').on('show.bs.modal', function() {
      var $title = $(this).find('.placeholder-title'),
          $body = $(this).find('.placeholder-body');
      $title.html($(this).data('title'));
      $body.html($(this).data('body'));
    });
    $(document).on('click', '[data-toggle="modal-information"]', function(e) {
      e.preventDefault();
      $($(this).attr('href')).data('title', $(this).data('modal-title'));
      $($(this).attr('href')).data('body', $(this).data('modal-body'));
      $($(this).attr('href')).modal('show');
    });

    // Abilita Tooltips
    $('[data-hover="tooltip"]').tooltip();

  });
})(jQuery);

/*
 *  INPUTS
 *  Le select vengono gestite da Chosen.
 *  Altri input e funzioni correlate sono:
 *    - input caricamento file e immagini.
 *    - generatore password.
 *    - visibilità con checkbox o select.
 *    - checkbox che selezionano/deselezionano tutto.
 *    - elementi ordinabili con trascinamento.
 *    - duplicatori elementi.
 *    - rimuovitori elementi.
 *    - liste, con possibilità di aggiungere, togliere od ordinare
 *        i rispettivi elementi.
 *    - form con autosubmit.
 *                                                                          */

(function($) {

  $.extend({
    material_design_chosen: function(select) {
      // Abilita le floating labels anche per le select con chosen
      function updateLabel(e) {
        var select = $(e.target);
        if ((select.val() == '' && select.find('option:selected').text() == '') ||
              select.val() == null || select.find('option:selected').size() <= 0) {
          select.parents('.form-group').removeClass('is-filled');
        } else {
          select.parents('.form-group').addClass('is-filled')
        }
      }

      select.chosen();
      select.on('change reset chosen:updated', updateLabel);
      select.on('chosen:update:label', updateLabel)
      select.on('chosen:showing_dropdown', function() {
        select.parents('.form-group').addClass('is-focused');
      });
      select.on('chosen:hiding_dropdown', function() {
        select.parents('.form-group').removeClass('is-focused');
      });
      select.trigger('chosen:update:label');
    },
    material_design_init: function() {
      { // Select

        // Attiva chosen su select
        $('[data-toggle="chosen-select"]').each(function() {
          $.material_design_chosen($(this))
        });
  
        // Select concorrenti
        $('[data-concurrent-select]').on('change', function() {
          var value = $(this).val(),
              concurrentName = $(this).attr('data-concurrent-select'),
              concurringSelects = $('[data-concurrent-select="' + concurrentName + '"]');
          
          concurringSelects.not($(this)).val(null).trigger('chosen:updated').trigger('reset');
          $(window).trigger('concurrent:change', [ $(this), $(this).val() ]);
        });
  
      }
  
      { // Input caricamento file e immagini
  
        // Input file multipli
        $('input[type="file"]').on('change', function() {
          var file_input = $(this),
              filename_input = $(this).next().find('input[type="text"]');
          if (file_input[0].files.length > 0) {
            file_names = []
            for(let file of file_input[0].files) {
              file_names.push(file.name)
            }
            filename_input.val(file_names.join(', '));
          } else {
            filename_input.val(null);
          }
        });

        // Preview immagini
        $('input[type="file"][data-toggle="image-preview"]').on('change', function() {
          let image_input = $(this),
              image_previews = $(`#${$(this).attr('id')}_preview`)
          
          image_previews.html(null)
          for(let file of image_input.get(0).files) {
            let reader = new FileReader()
            reader.onload = function (e) {
              image_previews.append(`
                <img
                  src="${e.target.result}"
                  class="img-rounded img-responsive img-raised margin-auto"/>`)
            }
            reader.readAsDataURL(file)
          }
        })
  
      }
  
      { // Generatore password
  
        function randomString(length) {
          return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length)))
            .toString(36).slice(1);
        }
        $('[data-password-generator]').click(function() {
          var password_field = $(this).data('password-generator');
          $('input[name="' + password_field + '"]')
            .val(randomString(8))
            .parents('.form-group').addClass('is-filled');
        });
  
      }
  
      { // Visibilità con checkbox o select
  
        function triggerVisibility(trigger) {
          var trigger_name = trigger.data('visible-trigger'),
              elements = $('[data-visible-on="' + trigger_name + '"]');

          if (trigger.is('input[type="checkbox"]')) {
            if (trigger.prop('checked'))
              elements.removeClass('hidden');
            else
              elements.addClass('hidden');
          } else if (trigger.is('select')) {
            elements.addClass('hidden');
            if (trigger.val()) {
              var valued_elements = elements.filter('[data-visible-on-value="' + trigger.val() + '"]')
              if (valued_elements.size() > 0)
                valued_elements.removeClass('hidden');
              else
                elements.removeClass('hidden')
            }
          }
        }
        $('[data-visible-trigger]').each(function() {
          triggerVisibility($(this));
        }).on('change reset', function() {
          triggerVisibility($(this));
        })
  
      }
  
      { // Checkbox seleziona tutto / deseleziona tutto
  
        $('[data-select-all]').on('change', function() {
          var checkbox = $(this),
              checkboxes_selector = checkbox.data('select-all');
          
          $('[data-select-all-on="' + checkboxes_selector + '"]')
            .prop('checked', checkbox.prop('checked'))
            .trigger('change');
        });
        $('[data-select-all-on]').on('change', function() {
          var checkbox = $(this),
              checkboxes_selector = checkbox.data('select-all-on'),
              siblings_checkboxes = $('[data-select-all-on="' + checkboxes_selector + '"]'),
              select_all_checkbox = $('[data-select-all="' + checkboxes_selector + '"]');
  
          if (siblings_checkboxes.filter(':checked').size() == siblings_checkboxes.size())
            select_all_checkbox.prop('checked', true);
          else
            select_all_checkbox.prop('checked', false);
        });
  
      }
    
      { // Elementi ordinabili con trascinamento
  
        $('[data-sortable]').sortable();
  
      }

      { // Duplicatori elementi
        $(document).on('click', '[data-duplicate]', function(event) {
          var duplicate_selector = $(event.target).closest('[data-duplicate]').data('duplicate'),
              element_to_duplicate = $(duplicate_selector);

          element_to_duplicate.parent()
            .append(element_to_duplicate.clone().removeClass('hidden').removeAttr('id'));
        });
      }

      { // Rimuovitori elementi
        $(document).on('click', '[data-remove]', function(event) {
          var remove_selector = $(event.target).closest('[data-remove]').data('remove');

          $(event.target).parents(remove_selector).remove();
        });
      }
  
      { // Liste con aggiunta, rimozione e ordinamento
  
        $('[data-list-add]').click(function() {
          var list_name = $(this).data('list-add'),
              list = $('[data-list="' + list_name + '"]'),
              template = $('[data-list-template="' + list_name + '"]');
          
          var template_copy = template.clone()
            .removeAttr('data-list-template')
            .attr('data-list-item', list_name)
            .removeClass('hidden');
          list.append(template_copy);
          template_copy.find('select')
            .chosen('destroy')
            .siblings('.chosen-container').remove()
            .end().each(function() {
              triggerChosen($(this))
            });
        });
        $(document).on('click', '[data-list-remove]', function() {
          var list_name = $(this).data('list-remove'),
              template = $(this).parents('[data-list-item="' + list_name + '"]');
          
          template.remove();
        });
        
      }
    }
  })

  $(document).ready(function() {
    $.material_design_init();
  });

})(jQuery)