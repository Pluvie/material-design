Vue.component('table-filter', {
  template: `
    <div class="table-filters-container">
      <div class="table-filters">
        <slot name="filters" :filters="filters"/>
      </div>
      <div class="row align-items-center mb-2">
        <div class="col text-left">
          <div class="d-flex">
            <span>
              {{ rowsCountText }}
              <b>{{ filteredRows.length }}</b>
            </span>
            <span>
              &nbsp;/&nbsp;
              {{ rowsPerPageText }}
              <select v-model="selectedRowsPerPage" dir="rtl">
                <option value="0">{{ showAllRowsText }}</option>
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
              </select>
            </span>
            <i class="material-icons select-arrow">arrow_drop_down</i>
          </div>
        </div>
        <div class="col text-right">
          <slot name="new-row"></slot>
          <button
            type="button"
            class="btn btn-outline-primary btn-sm"
            @click="exportCSV">esporta</button>
        </div>
      </div>
      <table class="table" :class="classes">
        <thead class="thead-light">
          <tr v-for="columnGroup in columns">
            <th
              v-for="column in columnGroup"
              :class="[ column.class, 'sortable', columnSortedClass(currentSort, column.id) ]"
              :colspan="column.colspan || 1"
              @click="sortColumn(column.id)">
              {{ column.text }}
            </th>
          </tr>
        </thead>
        <template v-if="autoCycleRows">
          <tbody>
            <tr v-for="(row, index) in pagedRows" :key="row.id">
              <slot name="row" :row="row"/>
            </tr>
          </tbody>
        </template>
        <template v-else>
          <slot name="rows" :rows="pagedRows"/>
        </template>
      </table>
      <ul class="pagination pagination-primary mt-2 justify-content-end">
        <li class="page-item">
          <a @click.prevent="prevPage()" class="page-link">
            {{ prevPageButtonText }}
          </a>
        </li>
        <li class="page-item" v-for="page in shownPages" :class="{ active: currentPage == page }">
          <a @click.prevent="goToPage(page)" class="page-link">
            {{ page + 1 }}
          </a>
        </li>
        <li class="page-item">
          <a @click.prevent="nextPage()" class="page-link">
            {{ nextPageButtonText }}
          </a>
        </li>
      </ul>
    </div>
  `,
  data() {
    return {
      currentPage: 0,
      currentSort: this.sortBy,
      selectedRowsPerPage: this.initialRowsPerPage.toString(),
    }
  },
  props: {
    classes: Array,
    filters: Object,
    rows: Array,
    columns: {
      type: Array,
      required: true
    },
    autoCycleRows: {
      type: Boolean,
      default: true
    },
    initialRowsPerPage: {
      type: Number,
      default: 25
    },
    sortBy: Object,
    rowsCountText: {
      type: String,
      default: 'Numero record trovati:'
    },
    rowsPerPageText: {
      type: String,
      default: 'Mostrati per pagina:'
    },
    showAllRowsText: {
      type: String,
      default: 'Tutti'
    },
    prevPageButtonText: {
      type: String,
      default: '<'
    },
    nextPageButtonText: {
      type: String,
      default: '>'
    }
  },
  computed: {
    pagesCount() {
      if (this.rowsPerPage != 0)
        return Math.floor(this.filteredRows.length / this.rowsPerPage, 10)
      else
        return 0
    },
    shownPages() {
      let pages = [ this.currentPage ]
      // 1 pagina prima
      if (this.currentPage > 0)
        pages.splice(0, 0, this.currentPage - 1)
      // 2 pagine prima
      if (this.currentPage > 1)
        pages.splice(0, 0, this.currentPage - 2)
      // 1 pagina dopo
      if (this.currentPage < this.pagesCount)
        pages.splice(pages.length, 0, this.currentPage + 1)
      // 2 pagine dopo
      if (this.currentPage < this.pagesCount - 1)
        pages.splice(pages.length, 0, this.currentPage + 2)
      
      return pages
    },
    rowsPerPage() {
      return parseInt(this.selectedRowsPerPage, 10)
    },
    filteredRows() {
      // Reimposta pagina a 1 quando viene cambiato qualsiasi filtro
      this.currentPage = 0
      // Mostra righe in base ai filtri
      return this.rows.filter(row => {
        let showRow = true
        Object.keys(this.filters).forEach(filter => {
          let filterValue = (this.filters[filter].value || '').toLowerCase(),
              filterMatching = this.filters[filter].matching || 'equality',
              filterType = this.filters[filter].type,
              rowValue = (row[filter] || '').toLowerCase()
          if (filterValue) {
            switch(filterType) {
              case String:
                if (filterMatching == 'equality') {
                  if (rowValue != filterValue)
                    showRow = false
                } else if (filterMatching == 'inclusion') {
                  if (rowValue.indexOf(filterValue) < 0)
                    showRow = false
                }
                break;
            }
          }
        })
        return showRow
      })
    },
    sortedRows() {
      // Ordina righe in base a intestazione
      if (this.currentSort && this.currentSort.id && this.currentSort.direction) {
        return this.filteredRows.sort((a, b) => {
          let inverter = this.currentSort.direction == 'asc' ? 1 : -1,
              aSortValue = a[this.currentSort.id],
              bSortValue = b[this.currentSort.id]
          if (aSortValue)
            aSortValue = aSortValue.toLowerCase()
          if (bSortValue)
            bSortValue = bSortValue.toLowerCase()
          
          if (aSortValue < bSortValue) return -1 * inverter
          if (aSortValue > bSortValue) return 1 * inverter
          return 0;
        })
      } else {
        return this.filteredRows.sort((a, b) => {
          if (a.id < b.id) return -1
          if (a.id > b.id) return 1
          return 0
        })
      }
    },
    pagedRows() {
      if (this.pagesCount > 0) {
        return this.sortedRows.filter((row, index) => {
          let pageIndexStart = this.rowsPerPage * this.currentPage,
              pageIndexStop = pageIndexStart + this.rowsPerPage
          if (index >= pageIndexStart && index < pageIndexStop)
            return true
          else
            return false
        })
      } else {
        return this.filteredRows
      }
    }
  },
  methods: {
    columnSortedClass(sort, id) {
      if (sort && id && sort.id == id)
        switch(sort.direction) {
          case 'asc':
            return { 'sorted-asc': true }
          case 'desc':
            return { 'sorted-desc': true }
          default:
            return null
        }
      else
        return null
    },
    sortColumn(id) {
      if (id) {
        if (this.currentSort && this.currentSort.id == id) {
          // Esisteva già un ordinamento attivo su questa colonna
          switch(this.currentSort.direction) {
            // Se era ascendente, effettua nuovo ordinamento su stessa colonna
            // ma in direzione discendente
            case 'asc':
              this.currentSort = {
                id: id,
                direction: 'desc'
              }
              break;
            // Se era discendente, elimina l'ordinamento sulla colonna
            case 'desc':
              this.currentSort = {}
              break;
            // Negli altri casi, effettua nuovo ordinamento
            // sulla stessa colonna in direzione ascendente
            default:
              this.currentSort = {
                id: id,
                direction: 'asc'
              }
          }
        } else {
          // Effettua nuovo ordinamento in direzione ascendente
          this.currentSort = {
            id: id,
            direction: 'asc'
          }
        }
      }
    },
    exportCSV() {
      let file = 'data:text/csv;charset=utf-8,',
          headers = this.columns[this.columns.length - 1]
            .map(column => column.text).join(','),
          content = this.filteredRows.map(row => {
            return this.columns[this.columns.length - 1]
              .map(column => {
                if (column.id) {
                  let value = row[column.id]
                  if (value && typeof value === String)
                    return value.replace(/,/g, '-')
                  else
                    return value
                }
              })
              .join(',')
          }),
          csvFile = encodeURI([ file, headers, content ].flat().join('\n'))
          downloadLink = document.createElement('a')

      downloadLink.setAttribute('href', csvFile)
      downloadLink.setAttribute('class', 'hidden')
      downloadLink.setAttribute('download', 'export.csv')
      document.body.appendChild(downloadLink)
      downloadLink.click()
    },
    nextPage() {
      if (this.currentPage < this.pagesCount)
        return this.currentPage++
      else
        return this.currentPage
    },
    prevPage() {
      if (this.currentPage > 0)
        return this.currentPage--
      else
        return this.currentPage
    },
    goToPage(index) {
      this.currentPage = index
    }
  },
  watch: {
    selectedRowsPerPage(previousValue, currentValue) {
      // Torna a prima pagina se è stato cambiato il numero
      // di righe da mostrare per pagina
      this.currentPage = 0
    }
  }
})
