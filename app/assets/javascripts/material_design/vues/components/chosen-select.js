Vue.component('chosen-select', {
  props: {
    value: [ String, Array ],
    multiple: Boolean
  },
  template: `<select :multiple="multiple" data-toggle="chosen-select"><slot></slot></select>`,
  watch: {
    value: function(val) {
      $(this.$el).val(val).trigger('chosen:updated');
    }
  },
  mounted: function() {
    // Inizializza chosen se non già presente
    if (!$(this.$el).data('chosen'))
      $.material_design_chosen($(this.$el))
    // Attiva l'evento cambio valore
    $(this.$el)
      .on("change", e => this.$emit('input', $(this.$el).val()))
    // Spara fuori valore iniziale
    this.$emit('input', $(this.$el).val())
  },
  destroyed: function() {
    $(this.$el).chosen('destroy');
  }
});
