Vue.component('base-link', {
  template: `
    <a :href="this.to" :class="this.classes">
      <slot></slot>
    </a>
  `,
  props: {
    to: {
      type: String,
      required: true
    },
    classes: [ Object, Array ]
  }
})
